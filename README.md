# XIcon图标选择器

#### 介绍
基于[iconHhysFa](https://gitee.com/luckygyl/iconFonts)图标选择器1.0改造， [layui](https://www.layui.com/doc/element/icon.html) 图标 140个，
font-awesome 有786个图标。

#### 演示地址
[https://imlzw.gitee.io/xicon](https://imlzw.gitee.io/xicon)

#### 组件预览
![输入图片说明](https://images.gitee.com/uploads/images/2020/1027/145314_0454fa52_432156.gif "xicon.gif")

#### 使用教程

1. 将xIcon放置layui的模块目录下，类似（****/lay/modules/)
2. 在引用的主html页面引入awesome的css文件,类似
```
<!-- ps: 如果仅用layui图标可不引 -->
<link rel="stylesheet" href="./xIcon/font-awesome/css/font-awesome.css" media="all">
```
3. 初始化表单text

```
<div class="layui-form-item">
	<label for="" class="layui-form-label">图标选择：</label>
	<div class="layui-input-block">
		<input type="text" id="demo1" lay-filter="" class="hide">
	</div>
</div>
```

4. 引用xIcon组件渲染

```
<script>
	layui.use(['xIcon'], function() {
		var xIcon = layui.xIcon;
		xIcon.render({
			// 选择器，推荐使用input
			elem: '#demo1',
			// 数据类型：layui/awesome，推荐使用layui
			type: 'layui,awesome',
			// 是否开启搜索：true/false，默认true
			search: true,
			// 是否开启分页：true/false，默认true
			page: true,
			// 每页显示数量，默认12
			limit: 999,
                        // 支持js初始值设置，优先于input value
                        // initValue: 'layui-icon-snowflake',
			// 点击回调
			click: function(data) {
				console.log(data);
			},
			// 渲染成功后的回调
			success: function(d) {
				console.log(d);
			}
		});
	});
</script>

```

#### 特别鸣谢

1. [iconHhysFa](https://gitee.com/luckygyl/iconFonts)
